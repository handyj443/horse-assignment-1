// THE SNAIL TRAIL GAME - Instrumented Version
// This version 1 is used for:
// - saving timing data to a file, 'SnailTrailTimes.csv', not just the screen.
// - producing averages for the various timing data.
// - saving key presses to a 'SnailTrailMoves.csv' file for subsequent automated
// game play (see version 2).

// A.Oram 2015, originally based on the P.Vacher's skeletal program.
// Comments showing 'NEW' are on lines additional to the original Snail Trail
// version.
/*
A snail (the player) moves about the garden leaving a trail of slime that it
doesn't like to cross, but this does dissolve in due course.
Some swine has scattered a number of hard-to-see slug pellets around and if the
snail slithers over a number of these it dies.
The garden has frogs that leap about trying to make lunch of the snail, but may
jump over it by accident. They may incidentally
soak up a slug pellet, thus helping the snail stay alive.
If they land on it or the snail runs into a frog, it's curtains for the snail.
Frogs also have a certain % chance of being carried off by a hungry eagle, so
the snail may live to see another day.
In this version there are a number of lettuces to eat - eat them all before
being eaten and the snail wins!

This code is certainly snail-like, can you get the frame rate up?


Screenshot of console during gameplay (mono colour!):
___________________________________________________________________________
...THE SNAIL TRAIL...
DATE: 13/11/2015
++++++++++++++++++++++++++++++   TIME: 18:28:23
+             -              +
+                           @+
+         @                  +
+                   -        +   Initialise game= 7.2715us
+                            +   Paint Game=      42.878ms
+                            +
+                            +   Frames/sec=      23.3 at 42.885ms/frame
+ -   -   .....              +
+    M    .   .     -      - +
+         . - .              +
+       &..   .         @    +
+-    -       .              +   TO MOVE USE ARROW KEYS - EAT ALL LETTUCES (@)
+             .              +   TO QUIT USE 'Q'
+    M        .              +
+             .  -           +   MSG:
+         -   .     -        +
+             .        -  -- +   SLITHERED OVER 1 PELLETS SO FAR!
+             .              +
++++++++++++++++++++++++++++++
___________________________________________________________________________

x co-ordinates follow this pattern (y co-ordinates similar):

0,1,2 ...    ...SIZEX-2, SIZEX-1

Garden interior extends 1..SIZEX-2, 1..SIZEY-2.
Walls are at 0 and SIZEX-1, 0 and SIZEY-1.

Annoying bleeps from the PC's speaker announce significant events, check the
message shown to see what happened.

*/

//_________________________________________________________________
// INCLUDES
#define BLEEP // turn annoying bleep on or off
#define TESTING // testing switch for convenience

#define UNRANDOMISE // always use the same seed?
#ifdef TESTING
#define READ_INPUT_FROM_FILE
#define TESTHARNESS // record and output moves and times?
//#define SKIP_PAINT // don't do any console output for speed when testing
#define ITERATIONS 1 // run the same input file lots of times for testing
#endif

// include libraries
// include standard libraries
//#include <iostream> //for output and input
#include <iomanip>   //for formatted output in 'cout'
#include <cstdio>
#include <conio.h>   //for getch
#include <cstring>
#include <fstream>   //for files
#include <string>    //for string
#include <vector>
#include <tchar.h>
#include "hr_time.h" //for timers

using namespace std;

// include our own libraries
#include "RandomUtils.h"  //for Seed, Random,
#include "ConsoleUtils.h" //for Clrscr, Gotoxy, etc.
#include "TimeUtils.h"    //for GetTime, GetDate, etc.

//_________________________________________________________________
// CONSTANTS

// garden dimensions
const int SIZEY(20); // vertical dimension
const int SIZEX(30); // horizontal dimension

const int MSG_LENGTH(40);

// constants used for the garden & its inhabitants
const char SNAIL('&');     // snail (player's icon)
const char DEADSNAIL('o'); // just the shell left...
const char GRASS(' ');     // open space, grass
const char WALL('+');      // garden wall

const char SLIME('.');   // snail produce

const char
    PELLET('-'); // should be invisible, but test using a visible character.
const int NUM_PELLETS(15); // number of slug pellets scattered about
const int PELLET_THRESHOLD(
    5); // deadly threshold! Slither over this number and you die!

const char LETTUCE('@');      // a lettuce
const char NO_LETTUCE(GRASS); // guess!
const int LETTUCE_QUOTA(4); // how many lettuces you need to eat before you win.

const int NUM_FROGS(2);
const char FROG('M');
const char DEAD_FROG_BONES('X'); // Dead frogs are marked as such in their 'y'
                                 // coordinate for convenience
const int FROGLEAP(4);           // How many spaces do frogs jump when they move
// There's a 1 in 'nn' chance of an eagle strike on a frog
const int EagleStrike(30); 

// the keyboard arrow codes
const int UP(72);    // up key
const int DOWN(80);  // down key
const int RIGHT(77); // right key
const int LEFT(75);  // left key

// other command letters
const char QUIT('q');       // end the game
#ifdef BLEEP
const char Bleep('\a');     // annoying Bleep
const string Bleeeep("\a\a\a\a"); // very annoying Bleeps
#else
const char Bleep(' ');     // annoying Bleep
const string Bleeeep;
#endif
// define left margin for messages etc (avoiding garden)
const int MLEFT(SIZEX + 3); 

//_________________________________________________________________
// GLOBALS
// define a few global control constants
int snailStillAlive(true); // snail starts alive!
int pellets(0);            // number of times snail slimes over a slug pullet
int lettucesEaten(0);      // win when this reaches LETTUCE_QUOTA
bool fullOfLettuce(false); // when full and alive snail has won!

int key, newGame(!QUIT); // NEW Made global for convenience (see 'main' for
                         // original declaration)

CStopWatch InitTime, FrameTime, PaintTime; // create stopwatchs for timing
//char garden[SIZEY][SIZEX];       // the game 'world'
char gardenStream[SIZEY * (SIZEX + 1)];
string bleeps;

//_________________________________________________________________
// GLOBAL CLASSES
struct Slime
{
	Slime() : x(0), y(0), life(-1) {}
	Slime(int x, int y, int life) : x(x), y(y), life(life) {}
	int x;
	int y;
	int life;
};

struct SlimeManager
{
	// how long slime lasts (in keypresses)
	const static int SLIMELIFE = 25;

	SlimeManager() : slimeTrailIndex(0) {
		slimeTrail.resize(SLIMELIFE);
	}

	void addSlime(int x, int y) {
		slimeTrail[slimeTrailIndex] = { x, y, SLIMELIFE };
		slimeTrailIndex = (slimeTrailIndex + 1) % SLIMELIFE;
	}

	void dissolveSlime() {
		for (Slime &s : slimeTrail) {
			if (--s.life == 0) {
				gardenStream[s.y * (SIZEX+1) + s.x] = GRASS;
			}
		}
	}

	vector<Slime> slimeTrail;
	int slimeTrailIndex;
};

struct Lettuce
{
	int x;
	int y;
	bool show;
};

//_________________________________________________________________
// TEST HARNESS
#ifdef TESTHARNESS
// NEW Declarations dealing with instrumentation and saving game data
ofstream ST_Times;                 // NEW
ofstream ST_Moves;                 // NEW
ofstream ST_Summary;
bool InitTimesAlreadySaved(FALSE); // NEW
char moveResult(0); // NEW saves result of snail's last  move for output file
char gameEvent(0);  // NEW saves any event such as Frog being eaten by eagle for
                    // output to file.
const char WIN('W');   // NEW
const char STUCK('S'); // NEW

double InitTimeTotal(0.);  // NEW
double FrameTimeTotal(0.); // NEW
double PaintTimeTotal(0.); // NEW
int GamesPlayed(0);        // NEW
int TotalMovesMade(0);     // NEW
#endif

#ifdef READ_INPUT_FROM_FILE
ifstream ST_MovesIn; // NEW
#endif               // READ_INPUT_FROM_FILE

//_____________________________________________________________________________
// FUNCTION PROTOTYPES
void initialiseGame(SlimeManager&, vector<Lettuce>&, int[],
                    int[][2], int &, int &, bool &);

void showTitle(int, int);
void showDateAndTime(int, int);
void showTimingHeadings(int, int);
void paintGarden();
void showOptions(int, int);
void showMessage(string&, int, int);
void showPelletCount(int, int, int);

void paintGame(int, string &message);
void clearMessage(string &message);

int getKeyPress();
void analyseKey(int, int move[2], string &message);
void moveFrogs(const vector<Lettuce>&, int[], int[][2], string &);
void moveSnail(SlimeManager&, vector<Lettuce>&, int[], int[], int &,
               string &);

int anotherGo(int, int);
int toIndex(int row, int col);

// Timing info
void showTimes(double, double, double, int, int);

#ifdef TESTHARNESS
void saveTimes(double, double, double, int, string, bool); // NEW
void openFiles(void);                                // NEW
#endif                                               // TESTHARNESS

#ifdef READ_INPUT_FROM_FILE
void openInputFile();
#endif

//_____________________________________________________________________________
// MAIN PROGRAM START
int main() {
	SlimeManager slime;
    //char lettucePatch[SIZEY][SIZEX]; // remember where the lettuces are planted
	vector<Lettuce> lettuces(LETTUCE_QUOTA);

    string message; // various messages are produced on screen

    int snail[2];            // the snail's current position (x,y)
    int frogs[NUM_FROGS][2]; // coordinates of the frog contingent n * (x,y)
    int move[2];             // the requested move direction

    //_____________________________________________________________
    // GAME START
#ifdef TESTHARNESS
	openFiles(); // NEW - open files to record moves and times
	bool finished = false;
#endif
#ifdef READ_INPUT_FROM_FILE
	openInputFile(); // open file containing previously recorded moves
#endif

#ifdef ITERATIONS
	for (int i = 0; i < ITERATIONS; ++i) {
		//OutputDebugString(_T("Iteration\n"));
#endif
	//_____________________________________________________________
	// MAIN GAME LOOP
		Seed(); // seed the random number generator
		while ((newGame | 0x20) != QUIT) // keep playing games
		{
			Clrscr();
#ifdef TESTHARNESS
			InitTimesAlreadySaved =
				FALSE; // NEW - only ouptut Init timing once per game
#endif

			InitTime.startTimer();
			// initialise garden (incl. walls, frogs, lettuces & snail)
			initialiseGame(slime, lettuces, snail, frogs, pellets,
						   lettucesEaten, fullOfLettuce);
			message = "READY TO SLITHER!? PRESS A KEY...";
			InitTime.stopTimer();

			// display static game graphics
			showTitle(0, 0);                     // display game title
			showTimingHeadings(MLEFT, 5);        // display Timings Heading
			showOptions(MLEFT, 14);              // display menu options available

			// display game space for the first time
			paintGame(pellets, message);

			key = getKeyPress();

			//_____________________________________________________________
			// FRAME LOOP
			// user not bored, and snail not dead or full
			while (((key | 0x20) != QUIT) && snailStillAlive &&
				   !fullOfLettuce) {

				FrameTime.startTimer();
				//_____________________________________________________________
				// TIMER START
				analyseKey(key, move, message);
				moveSnail(slime, lettuces, snail, move, pellets,
						  message);

				// remove slime from garden
				slime.dissolveSlime();

				// draw snail in garden
				gardenStream[toIndex(snail[0], snail[1])] = SNAIL;

				// frogs attempt to home in on snail
				if (snailStillAlive) {
					moveFrogs(lettuces, snail, frogs, message);
				}

				//_____________________________________________________________
				// TIMER STOP
				FrameTime.stopTimer();

				// display game info, garden & messages
				paintGame(pellets, message);

#ifndef TESTHARNESS
				clearMessage(message);
#endif
				showTimes(InitTime.getElapsedTime(), FrameTime.getElapsedTime(),
						  PaintTime.getElapsedTime(), MLEFT, 6);
#ifdef TESTHARNESS
				// NEW Save performance data outside of game loop
				saveTimes(InitTime.getElapsedTime(), FrameTime.getElapsedTime(),
						  PaintTime.getElapsedTime(), key, message, finished);
				// NEW reset message array, moved from above 
				// (so we an use it above in saveTimes)
				clearMessage(message);
				// NEW prevent repeated saving of Init timing
				InitTimesAlreadySaved = TRUE; 
#endif
				key = getKeyPress(); // display menu & read in next option
			}

			//_____________________________________________________________
			// END OF FRAME LOOP
			(snailStillAlive) ? message = "WELL DONE, YOU'VE SURVIVED"
				              : message = "REST IN PEAS.";

			if (!snailStillAlive) {
				gardenStream[toIndex(snail[0], snail[1])] = DEADSNAIL;
			}
			// display final game info, garden & message
			paintGame(pellets, message); 
			newGame = anotherGo(MLEFT, 19); // play again, or Quit game.
		}
		//_____________________________________________________________
		// END OF MAIN GAME LOOP

#ifdef ITERATIONS
		// reset input file stream to beginning
		ST_MovesIn.clear();
		ST_MovesIn.seekg(0, ios::beg);
		newGame = 0; // play again
	}
#endif

#ifdef TESTHARNESS
	finished = true;
    // NEW Report final frame time (only the paintGame time will be different
    // from last set as frame won't be recalculated).
    saveTimes(InitTime.getElapsedTime(), FrameTime.getElapsedTime(),
              PaintTime.getElapsedTime(), key, message, finished); // NEW
#endif

    return 0;
}

//_____________________________________________________________________________
// FUNCTION DEFINITIONS
void scatterPelletsAndLettuce(vector<Lettuce>&, int[]);
void scatterFrogs(int[], int[][2]);
//_________________________________________________________________
// Set game configuration
void initialiseGame(SlimeManager &slime,
                    vector<Lettuce> &lettuces, int snail[], int frogs[][2],
                    int &pellets, int &Eaten,
                    bool &fullUp) {

	strcpy(gardenStream , "++++++++++++++++++++++++++++++\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "+                            +\n"
	                      "++++++++++++++++++++++++++++++");

	// position the snail at random inside garden
	snail[0] = Random(SIZEY - 2); // vertical coordinate in range [1..(SIZEY - 2)]
	snail[1] = Random(SIZEX - 2); // horizontal coordinate in range [1..(SIZEX - 2)]

	// show snail in garden
	gardenStream[toIndex(snail[0], snail[1])] = SNAIL;

	// randomly place lettuces and pellets in garden
    scatterPelletsAndLettuce(lettuces, snail); 

	// randomly place a few frogs around
    scatterFrogs(snail, frogs); 

    pellets = 0;    // no slug pellets slithered over yet
    Eaten = 0;      // reset number of lettuces eaten
	snailStillAlive = true;
    fullUp = false; // snail is hungry again
}

//_________________________________________________________________
// Scatter some stuff around the garden (slug pellets and lettuces)
inline void scatterPelletsAndLettuce(vector<Lettuce> &lettuces, int snail[]) {
	// scatter some slug pellets
	for (int slugP = 0; slugP < NUM_PELLETS; slugP++) {
		// prime x,y coords with initial random numbers before checking
		int x(Random(SIZEX - 2)),
			y(Random(SIZEY - 2));
		// ensure stuff doesn't land on the snail, or each other.
		while (((y = Random(SIZEY - 2)) == snail[0]) &&
			   ((x = Random(SIZEX - 2)) == snail[1]) ||
			   gardenStream[toIndex(y, x)] == PELLET) {
			;
		}
		gardenStream[toIndex(y, x)] = PELLET;
	}

	// scatter lettuce
	int numLettuces = 0;
	while (numLettuces < LETTUCE_QUOTA) {
		int x = Random(SIZEX - 2);
		int y = Random(SIZEY - 2);
		// check that there isn't already a pellet or a snail here
		if (gardenStream[toIndex(y, x)] == GRASS) {
			// space is free - place lettuce here
			lettuces[numLettuces].x = x;
			lettuces[numLettuces].y = y;
			lettuces[numLettuces].show = true;
			gardenStream[toIndex(y, x)] = LETTUCE;
			numLettuces++;
		}
	}
}

//_________________________________________________________________
//
inline void scatterFrogs(int snail[], int frogs[][2]) {
	// Need to avoid the snail initially (seems a bit unfair otherwise!). Frogs
	// aren't affected by slug pellets and will absorb them.
	for (int f = 0; f < NUM_FROGS; f++) {
		// prime coords before checking
		int x = Random(SIZEX - 2);
		int y = Random(SIZEY - 2);
		// avoid snail and existing frogs
		while (((y = Random(SIZEY - 2)) == snail[0]) &&
			   ((x = Random(SIZEX - 2)) == snail[1]) ||
			   gardenStream[toIndex(y, x)] == FROG) {
			;
		}
		// store initial positions of frog
		frogs[f][0] = y; 
		frogs[f][1] = x;
		// put frogs on garden (this may overwrite a slug pellet)
		gardenStream[toIndex(frogs[f][0], frogs[f][1])] = FROG; 
	}
}

//_________________________________________________________________
// Implement arrow key move
inline void analyseKey(int key, int move[2], string &msg) { 

    switch (key) //...depending on the selected key...
    {
    case LEFT: // prepare to move left
        move[0] = 0;
        move[1] = -1; // decrease the X coordinate
        break;
    case RIGHT: // prepare to move right
        move[0] = 0;
        move[1] = +1; // increase the X coordinate
        break;
    case UP: // prepare to move up
        move[0] = -1;
        move[1] = 0; // decrease the Y coordinate
        break;
    case DOWN: // prepare to move down
        move[0] = +1;
        move[1] = 0; // increase the Y coordinate
        break;
    default:                 // this shouldn't happen
        msg = "INVALID KEY"; // prepare error message
        move[0] = 0;         // move snail out of the garden
        move[1] = 0;
    }
}

//_________________________________________________________________
// Move the Frogs toward the snail - watch for eagles!
inline void moveFrogs(const vector<Lettuce> &lettuces, int snail[],
                      int frogs[][2], string &msg) {
    // Frogs move toward the snail. They jump 'n' positions at a time in either
    // or both x and y directions. If they land on the snail then it's dead meat.
    // They might jump over it by accident.
    // They can land on lettuces and slug pellets - in the latter case the
    // pellet is absorbed harmlessly by the frog (thus inadvertently helping
    // the snail!). 
	// Frogs may also be randomly eaten by an eagle, with only the bones left
    // behind.

    for (int f = 0; f < NUM_FROGS; f++) {
		// if frog not been gotten by an eagle
        if (frogs[f][0] != DEAD_FROG_BONES) {
            // jump off garden (taking any slug pellet with it)
			gardenStream[toIndex(frogs[f][0], frogs[f][1])] = GRASS;
			// check if frog is hiding a lettuce - linear search
			for (auto const &l : lettuces) {
				if (l.x == frogs[f][1] && l.y == frogs[f][0] && l.show) {
					gardenStream[toIndex(frogs[f][0], frogs[f][1])] = LETTUCE;
					break;
				}
			}

            // work out where to jump to depending on where the snail is...
            // see which way to jump in the Y direction (up and down)
            if (snail[0] - frogs[f][0] > 0) {
                frogs[f][0] += FROGLEAP;
				if (frogs[f][0] >= SIZEY - 1) {
					frogs[f][0] = SIZEY - 2;
				}
            } // don't go over the garden walls!
            else if (snail[0] - frogs[f][0] < 0) {
                frogs[f][0] -= FROGLEAP;
				if (frogs[f][0] < 1) {
					frogs[f][0] = 1;
				}
            };

            // see which way to jump in the X direction (left and right)
            if (snail[1] - frogs[f][1] > 0) {
                frogs[f][1] += FROGLEAP;
				if (frogs[f][1] >= SIZEX - 1) {
					frogs[f][1] = SIZEX - 2;
				}
            } else if (snail[1] - frogs[f][1] < 0) {
                frogs[f][1] -= FROGLEAP;
				if (frogs[f][1] < 1) {
					frogs[f][1] = 1;
				}
            };

			if (Random(EagleStrike) == EagleStrike) {
				// Frog killed by eagle
				gardenStream[toIndex(frogs[f][0], frogs[f][1])] = DEAD_FROG_BONES;
				// If the frog died on a lettuce, show the lettuce
				for (auto const &l : lettuces) {
					if (l.x == frogs[f][1] && l.y == frogs[f][0] && l.show) {
						gardenStream[toIndex(frogs[f][0], frogs[f][1])] = LETTUCE;
						break;
					}
				}
				frogs[f][0] = DEAD_FROG_BONES;
				msg = "EAGLE GOT A FROG";
				bleeps.push_back(Bleep);

#ifdef TESTHARNESS
				gameEvent = DEAD_FROG_BONES; // NEW record this event
#endif
			}
			else {
				// Frog not killed by eagle
				// If the frog landed on the snail, game over!
				if (frogs[f][0] == snail[0] && frogs[f][1] == snail[1]) {
					msg = "FROG GOT YOU!";
					bleeps.append(Bleeeep);
					snailStillAlive = false;
#ifdef TESTHARNESS
					gameEvent = FROG; // NEW record this event
#endif
					break;
				}
				else {
					// display frog on garden (thus destroying any
					// pellet that might be there).
					gardenStream[toIndex(frogs[f][0], frogs[f][1])] = FROG;
				}
			}
        }
    } // end of FOR loop
}

//_________________________________________________________________
// Implement player's move command
inline void moveSnail(SlimeManager &slime, vector<Lettuce> &lettuces, 
					  int snail[], int keyMove[], int &pellets, string &msg) {
    // move snail on the garden when possible.
    // check intended new position & move if possible...
    // ...depending on what's on the intended next position in garden.

    int targetY(snail[0] + keyMove[0]);
    int targetX(snail[1] + keyMove[1]);
    switch (gardenStream[toIndex(targetY, targetX)])
    {
    case GRASS:
    case DEAD_FROG_BONES: // its safe to move over dead/missing frogs too
        gardenStream[toIndex(snail[0], snail[1])] = SLIME;
		slime.addSlime(snail[1], snail[0]);
		snail[0] += keyMove[0]; // go in direction indicated by keyMove
        snail[1] += keyMove[1];

#ifdef TESTHARNESS
        moveResult = GRASS; // NEW record result of move
#endif
        break;

    case WALL:         // oops, garden wall
		bleeps.push_back(Bleep);
        msg = "THAT'S A WALL!";

#ifdef TESTHARNESS
        moveResult = WALL; // NEW record result of move
#endif

        break; //& stay put

    case FROG: //	kill snail if it throws itself at a frog!
        gardenStream[toIndex(snail[0], snail[1])] = SLIME;
        snail[0] += keyMove[0]; // go in direction indicated by keyMove
        snail[1] += keyMove[1];
        msg = "OOPS! ENCOUNTERED A FROG!";
		bleeps.append(Bleeeep);
        snailStillAlive = false; // game over
#ifdef TESTHARNESS
        gameEvent = DEADSNAIL; // NEW
        moveResult = FROG;     // NEW record result of move
#endif

        break;

    case LETTUCE: // increment lettuce count and win if snail is full
        gardenStream[toIndex(snail[0], snail[1])] = SLIME;
        slime.addSlime(snail[1], snail[0]); // set slime lifespan
        snail[0] += keyMove[0]; // go in direction indicated by keyMove
        snail[1] += keyMove[1];
		// find the lettuce that was eaten and mark it
		// linear search is acceptable because the array is small and
		// the LETTUCE case is rare
		for (auto &l : lettuces) {
			if (l.x == snail[1] && l.y == snail[0]) {
				l.show = false;
				break;
			}
		}
        lettucesEaten++;

		// if full, stop the game as snail wins!
        fullOfLettuce = (lettucesEaten == LETTUCE_QUOTA); 
        fullOfLettuce ? msg = "LAST LETTUCE EATEN" : msg = "LETTUCE EATEN";
		fullOfLettuce ? bleeps.append(Bleeeep) : bleeps.push_back(Bleep);

#ifdef TESTHARNESS
        if (fullOfLettuce)
            gameEvent = WIN;  // NEW
        moveResult = LETTUCE; // NEW record result of move
#endif
        break;

    case PELLET: // increment pellet count and kill snail if > threshold
        gardenStream[toIndex(snail[0], snail[1])] = SLIME;
        slime.addSlime(snail[1], snail[0]); // set slime lifespan
        snail[0] += keyMove[0]; // go in direction indicated by keyMove
        snail[1] += keyMove[1];
        pellets++;
		bleeps.push_back(Bleep);
		// aaaargh! poisoned!
        if (pellets >= PELLET_THRESHOLD) {
            msg = "TOO MANY PELLETS SLITHERED OVER!";
			bleeps.append(Bleeeep);
            snailStillAlive = false; 
#ifdef TESTHARNESS
            gameEvent = DEADSNAIL; // NEW
#endif
        }
#ifdef TESTHARNESS
        moveResult = PELLET; // NEW record result of move
#endif
        break;

    default: // must have encountered slime
        msg = "TRY A DIFFERENT DIRECTION";
#ifdef TESTHARNESS
        moveResult = STUCK; // NEW record result of move
#endif
    }
} // end of MoveSnail

//_____________________________________________________________________________
// HELPER AND DISPLAY FUNCTIONS

//_________________________________________________________________
// display game title, messages, snail & other elements on screen
void paintGame(int pellets, string &msg) {
#ifndef SKIP_PAINT
	PaintTime.startTimer();              // Time this function body
	showDateAndTime(MLEFT, 1);           // display system clock
	paintGarden();                       // display garden contents
	showPelletCount(pellets, MLEFT, 19); // display poisonous moves made so far
	showMessage(msg, MLEFT, 17);         // display status message, if any

	// output bleeps
	printf(bleeps.c_str());
	bleeps.clear();
	PaintTime.stopTimer();
#endif
}

//_________________________________________________________________
// display garden content on screen
inline void paintGarden() {
	SelectBackColour(clGreen);
	SelectTextColour(clDarkBlue);
	Gotoxy(0, 2);

	printf(gardenStream);
}

//_________________________________________________________________
// Get input command key
int getKeyPress() {
    int command = 0;
#ifdef READ_INPUT_FROM_FILE
    char scrap = 0;
    // get command from file
    ST_MovesIn >> command;
    ST_MovesIn >> scrap;
#else
    // get command from user

    // read in the selected option
    command = _getch();    // to read arrow keys
    while (command == 224) // to clear extra info from buffer
        command = _getch();
#ifdef TESTHARNESS
    ST_Moves << command
             << ','; // NEW save commands as they're entered, as CSV file.
#endif
#endif
    return (command);

}

//_________________________________________________________________
// Display info on screen
void clearMessage(string &msg) { 
	msg.assign(MSG_LENGTH, ' ');
}

inline void showTitle(int column, int row) {

    Clrscr();
    SelectBackColour(clBlack);
    SelectTextColour(clYellow);
    Gotoxy(column, row);
    printf("...THE SNAIL TRAIL...");
    SelectBackColour(clWhite);
    SelectTextColour(clRed);
}

inline void showDateAndTime(int column, int row) {

    SelectBackColour(clWhite);
    SelectTextColour(clBlack);
    Gotoxy(column, row);
    printf("DATE: %s", GetDate().c_str());
    Gotoxy(column, row + 1);
    printf("TIME: %s", GetTime().c_str());
}

inline void showOptions(int column, int row) {

    SelectBackColour(clRed);
    SelectTextColour(clYellow);
    Gotoxy(column, row);
    printf("TO MOVE USE ARROW KEYS - EAT ALL LETTUCES (%c)", LETTUCE);
    Gotoxy(column, row + 1);
    printf("TO QUIT USE 'Q'");
}

// display number of pellets slimed over
inline void showPelletCount(int pellets, int column, int row) {

    SelectBackColour(clBlack);
    SelectTextColour(clWhite);
    Gotoxy(column, row);
    printf("SLITHERED OVER %d PELLETS SO FAR!", pellets);
}

// display auxiliary messages if any
inline void showMessage(string &msg, int column, int row) {
	// pad msg out to MSG_LENGTH chars
	// UNSAFE if msg is longer than MSG_LENGTH chars already!!
	msg.append(string(MSG_LENGTH - msg.length(), ' '));

    SelectBackColour(clBlack);
    SelectTextColour(clYellow);
    Gotoxy(column, row);
    printf("MSG: %s", msg.c_str());
}

inline void showTimingHeadings(int column, int row) {
    SelectBackColour(clBlack);
    SelectTextColour(clYellow);
    Gotoxy(column, row);
    printf("Game Timings:");
}

// show end message and hold output screen
inline int anotherGo(int column, int row) {

    SelectBackColour(clRed);
    SelectTextColour(clYellow);
#ifndef SKIP_PAINT
    Gotoxy(column, row);
    printf("PRESS 'Q' TO QUIT OR ANY KEY TO CONTINUE");
#endif
    SelectBackColour(clBlack);
    SelectTextColour(clWhite);

    return (getKeyPress());
}

// show various times as a measure of performance
inline void showTimes(double InitTimeSecs, double FrameTimeSecs, double PaintTimeSecs,
               int column, int row) {
#ifndef SKIP_PAINT
#define milli (1000.)
#define micro (1000000.)

    SelectBackColour(clBlack);
    SelectTextColour(clWhite);
    Gotoxy(column, row);
    printf("Initialise game= %.4fus", InitTimeSecs * micro);
    Gotoxy(column, row + 1);
    printf("Paint Game=      %.4fms", PaintTimeSecs * milli);
    Gotoxy(column, row + 3);
    printf("Frames/sec=      %.1f at %.3fus/frame", 
		   (double)1.0 / FrameTimeSecs, FrameTimeSecs * micro);
#endif
}

inline int toIndex(int row, int col) {
	return row * (SIZEX+1) + col;
}

#ifdef TESTHARNESS
//_________________________________________________________________
// TESTHARNESS function
// save timing and other performance data to a file
void saveTimes(double InitTimeSecs, double FrameTimeSecs, double PaintTimeSecs,
               int key, string msg, bool finished) {
#define milli (1000.)
#define micro (1000000.)

    if (!InitTimesAlreadySaved) {
#ifndef ITERATIONS
        ST_Times << "\n\nDATE:\t" << GetDate() << "\tTIME:\t" << GetTime()
                 << setprecision(5) << "\nInitialising game took "
                 << InitTimeSecs * micro << "us"; // only do these once per game
        // Produce headings for rest of data
        ST_Times << "\n\nPaint Game (ms)\t\tMove made & Result\t\t\t\tFrame "
                    "rate/s\tTime/Frame (ms)\tScreen messages";
#endif
        InitTimeTotal += InitTimeSecs;
        GamesPlayed++;
    }

#ifndef ITERATIONS
    ST_Times << setprecision(5) << "\n\t" << PaintTimeSecs * milli;
    ST_Times << "\t\t\t"
             << (key == LEFT
                     ? "LEFT "
                     : key == RIGHT
                           ? "RIGHT"
                           : key == UP ? "UP   " : key == DOWN
                                                       ? "DOWN "
                                                       : key == QUIT ? "QUIT "
                                                                     : "Wrong")
             << " to " << (moveResult == GRASS
                               ? "GRASS  "
                               : moveResult == WALL
                                     ? "WALL   "
                                     : moveResult == PELLET
                                           ? "PELLET "
                                           : moveResult == LETTUCE ? "LETTUCE"
                                                                   : "STUCK  ")
             << " and "
             << (gameEvent == FROG
                     ? "FROG!!    "
                     : gameEvent == DEAD_FROG_BONES
                           ? "FROG EATEN"
                           : gameEvent == WIN
                                 ? "WON       "
                                 : gameEvent == DEADSNAIL ? "LOSE GAME "
                                                          : "still OK  ");

    gameEvent = 0; // reset status
    moveResult = 0;

    ST_Times << setprecision(3) << "\t\t\t" << (double)1.0 / FrameTimeSecs
             << "\t" << setprecision(5) << FrameTimeSecs * milli;
    ST_Times << "\t\t" << msg; // confirm messages seen on screen
#endif
    PaintTimeTotal += PaintTimeSecs;
    FrameTimeTotal += FrameTimeSecs;
    TotalMovesMade++; // for the whole session, may be several games.

    //if ((newGame | 0x20) == QUIT) { 
	if (finished) {
		// all games played, figure the averages and store
        ST_Times << "\n\nSUMMARY for " << GamesPlayed << " games played, with "
                 << TotalMovesMade << " moves entered.";
        ST_Times << setprecision(3) << "\n\nAverage frames/sec=\t"
                 << (double)TotalMovesMade / FrameTimeTotal
                 << "\nAverage Paint time=\t"
                 << (PaintTimeTotal * milli) / (double)TotalMovesMade << "ms"
                 << "\nAverage Init time=\t"
                 << (InitTimeTotal * micro) / (double)GamesPlayed << "us"
				 << "\nAverage Process time =\t"
				 << (FrameTimeTotal * micro) / (double)TotalMovesMade << " us";
        ST_Times << "\n\n******************************************************"
			        "*******************";

		ST_Summary << "DATE:\t" << GetDate() << "\tTIME:\t" << GetTime() << "\n";
		ST_Summary << "SUMMARY for " << GamesPlayed << " games played, with "
			<< TotalMovesMade << " moves entered.";
		ST_Summary << setprecision(3) 
				   << "\n\nAverage frames/sec   =\t"
				   << (double)TotalMovesMade / FrameTimeTotal
				   << "\nAverage Paint time   =\t"
				   << (PaintTimeTotal * milli) / (double)TotalMovesMade << " ms"
				   << "\nAverage Init time    =\t"
				   << (InitTimeTotal * micro) / (double)GamesPlayed << " us"
				   << "\nAverage Process time =\t"
				   << (FrameTimeTotal * micro) / (double)TotalMovesMade << " us";
		ST_Summary << "\n\n******************************************************"
	        		  "*******************\n\n";
    }
}
#endif

#ifdef TESTHARNESS
//_________________________________________________________________
// TESTHARNESS function
// Open the files that will store the times produced and the file with the
// moves recorded. File contents are overwritten.
// NB. Files are closed automatically by system when program exits (not good
// but sufficient here)
void openFiles() {
    ST_Times.open("SnailTrailTimes.csv");
#ifndef READ_INPUT_FROM_FILE
    ST_Moves.open("SnailTrailMoves.csv"); // open for writing moves to
#endif
	ST_Summary.open("SnailTrailSummary.txt", ofstream::app);
}

#ifdef READ_INPUT_FROM_FILE
//_________________________________________________________________
// TESTHARNESS function
// Open the moves file to automatically play previously recorded moves.
// NB. Files are closed automatically by system when program exits (not good
// but sufficient here)
void openInputFile() { 
	ST_MovesIn.open("SnailTrailMoves_FOR_DEMO_USE_999_SEED.csv"); 
}
#endif
#endif